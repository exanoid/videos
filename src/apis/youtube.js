import axios from 'axios';

const KEY = 'AIzaSyAAuA3s39nAYRa9vzD9dJ569RP5Jdqblbo';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        part: 'snippet',
        maxResults: 5,
        key: KEY
    }
});